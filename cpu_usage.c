/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////													     ////
////	This program will calculate average cpu usage 							     ////
////	since system boot using below formula:								     ////
////	     				  user + nice + system + irq + softirq + steal time		     ////
////	avg cpu usage percent = ----------------------------------------------------------------------- * 100////
////	     			  user + nice + system + irq + softirq + steal + idle + toawait time	     ////
////													     ////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

#define BUFF_SIZE 1
#define CPU_SCHEDULE 10


int main(){
	int input_file_desc;
	ssize_t returned_in;
	char buffer[BUFF_SIZE];
	unsigned int times[CPU_SCHEDULE];
	unsigned int temp = 0;
	unsigned int totall_time = 0;
	unsigned int totall_idle_time = 0;
	
	input_file_desc = open("/proc/stat", O_RDONLY);
	if (input_file_desc == -1){
		perror("openning problem");
	}
	
	lseek(input_file_desc, 5, SEEK_SET);
	
	int i = 0;
	while ((returned_in = read(input_file_desc, &buffer, BUFF_SIZE)) > 0){
		if (buffer[0] == '\n'){
			//printf("%d end", temp);
			times[i] = temp;
			break;
		} else {
			if (buffer[0] == ' '){
				//printf("%d\n", temp);
				times[i] = temp;
				i++;
				temp = 0;
			} else {
				temp *= 10;
				temp += buffer[0] - 48;
			}
		}
		buffer[0] = 0;
	}
	printf("1st column : user = normal processes executing in user mode : %d\n", times[0]);
	printf("2nd column : nice: niced processes executing in user mode : %d\n", times[1]);
	printf("3rd column : system: processes executing in kernel mode : %d\n", times[2]);
	printf("4th column : idle: twiddling thumbs : %d\n", times[3]);
	printf("5th column : iowait: waiting for I/O to complete : %d\n", times[4]);
	printf("6th column : irq: servicing interrupts : %d\n", times[5]);
	printf("7th column : softirq: servicing softirqs : %d\n", times[6]);
	printf("8th column : steal: involuntary wait : %d\n", times[7]);
	printf("9th column : guest: running a normal guest : %d\n", times[8]);
	printf("10th column : guest_nice: running a niced guest : %d\n", times[9]);
	for (int i = 0; i < 8; i++){
		totall_time += times[i];
	}
	totall_idle_time = times[3] + times[4];
	printf("cpu totall usage time is : %d and totall idle time is : %d\n", totall_time, totall_idle_time);
	printf("cpu useful time was: %d equal %f percent of cpu time.", totall_time - totall_idle_time,
	 ((double)(totall_time - totall_idle_time)/(double)(totall_time) * 100.00));
	close(input_file_desc);
	return 0;
}
