////////////////////////////////////////////////////////////////////////////////////////////////////////////
////													////
//// This Program will use sys/time.h in order to display current seconds and microseconds since EPOCH  ////
////											      		////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// written By smjfas and pkms on Aban 1396
#include <stdio.h>
#include <sys/time.h>

int main(){
    struct timeval my_time; 
    gettimeofday(&my_time, NULL);
    printf("the time passed from EPOCH is:\n%ld seconds\n%ld microseconds\n", my_time.tv_sec, my_time.tv_usec);
    return 0;
}
